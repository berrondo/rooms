[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://bitbucket.org/daniel-augusto/tauria_challenge)

## Tauria Challenge

To run the project, you need to copy the `local.env` to create your `.env` file:

`cp local.env .env`

And then, just:

`docker-compose up`

## Docs

The API Docs can be found at http://localhost:10010/

## Tests

To run the tests you just need to run:

`make test`