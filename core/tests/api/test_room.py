import json

from rest_framework import status
from rest_framework.reverse import reverse

from core.models import Room


class TestRoomViewSet:
    def test_retrieve_room(self, client, room):
        response = client.get(reverse("room-detail", kwargs={'guid': room.guid}))

        assert response.status_code == status.HTTP_200_OK
        data = response.json()
        assert data['name'] == room.name
        assert data['guid'] == str(room.guid)
        assert data['capacity'] == room.capacity
        assert data['host'] == room.host.username
        assert data['participants'] == [room.host.username]

    def test_create_a_room(self, client, user):
        payload = {
            'name': 'Test Chamber II',
            'capacity': 10,
        }

        client.force_login(user)
        response = client.post(
            reverse("room-list"),
            data=json.dumps(payload),
            content_type="application/json",
        )

        assert response.status_code == status.HTTP_201_CREATED
        data = response.json()
        room = Room.objects.last()
        assert data['guid']
        assert data['guid'] == str(room.guid)
        assert data['name'] == room.name == 'Test Chamber II'
        assert data['capacity'] == room.capacity == 10
        assert data['host'] == user.username
        assert data['participants'] == [user.username]
        assert room.host == room.participants.get() == user

    def test_creates_a_room_with_default_capacity(self, client, user):
        client.force_login(user)
        response = client.post(
            reverse("room-list"),
            data=json.dumps({'name': 'Test Chamber'}),
            content_type="application/json",
        )

        assert response.status_code == status.HTTP_201_CREATED
        room = Room.objects.get(guid=response.json()['guid'])
        assert room.host == user
        assert room.capacity == 5

    def test_creates_a_room_with_capacity_zero(self, client, user):
        client.force_login(user)
        response = client.post(
            reverse("room-list"),
            data=json.dumps({'name': 'Test Chamber', 'capacity': 0}),
            content_type="application/json",
        )

        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_create_room_unauthenticated(self, client):
        response = client.post(
            reverse("room-list"),
            data=json.dumps({'name': 'Test Chamber'}),
            content_type="application/json",
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_joins_the_room(self, client, room, user):
        client.force_login(user)

        response = client.patch(
            reverse("room-join", kwargs={'guid': str(room.guid)}),
        )

        room.refresh_from_db()

        assert response.status_code == status.HTTP_200_OK
        assert user in room.participants.all()

    def test_tries_to_join_unauthenticated(self, client, room):
        response = client.patch(
            reverse("room-join", kwargs={'guid': str(room.guid)}),
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_tries_to_join_a_full_room(self, client, room, user):
        room.capacity = room.occupation
        room.save()
        client.force_login(user)

        response = client.patch(
            reverse("room-join", kwargs={'guid': str(room.guid)}),
        )

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert 'The room is already full.' in response.json()

    def test_leaves_the_room(self, client, room, user):
        room.add_user(user)
        client.force_login(user)

        response = client.patch(
            reverse("room-leave", kwargs={'guid': str(room.guid)}),
        )

        room.refresh_from_db()

        assert response.status_code == status.HTTP_200_OK
        assert user not in room.participants.all()

    def test_tries_to_leave_unauthenticated(self, client, room, user):
        room.add_user(user)

        response = client.patch(
            reverse("room-leave", kwargs={'guid': str(room.guid)}),
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_changes_the_host(self, client, room, host, user):
        room.add_user(user)
        client.force_login(host)

        response = client.patch(
            reverse("room-host", kwargs={'guid': str(room.guid)}),
            data=json.dumps({'new_host': user.username}),
            content_type="application/json",
        )
        room.refresh_from_db()

        assert response.status_code == status.HTTP_200_OK
        assert room.host == user

    def test_tries_to_change_the_host_but_the_new_one_is_not_on_the_room(self, client, room, host, user):
        client.force_login(host)

        response = client.patch(
            reverse("room-host", kwargs={'guid': str(room.guid)}),
            data=json.dumps({'new_host': user.username}),
            content_type="application/json",
        )
        room.refresh_from_db()

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert room.host == host

    def test_non_host_tries_to_change_the_host(self, client, room, host, user):
        client.force_login(user)

        response = client.patch(
            reverse("room-host", kwargs={'guid': str(room.guid)}),
            data=json.dumps({'new_host': user.username}),
            content_type="application/json",
        )
        room.refresh_from_db()

        assert response.status_code == status.HTTP_403_FORBIDDEN
        assert room.host == host

    def test_tries_to_filter_by_user(self, client, room, host):
        response = client.get(
            f'{reverse("room-list")}?username={host.username}',
        )
        data = response.json()

        assert response.status_code == status.HTTP_200_OK
        assert len(data) == 1
        assert data[0]['name'] == 'Test Chamber'
        assert data[0]['guid'] == str(room.guid)
        assert data[0]['host'] == host.username
        assert data[0]['participants'] == [host.username]
        assert data[0]['capacity'] == 5

    def test_tries_to_filter_by_user_that_is_not_in_any_room(self, client, room, user):
        response = client.get(
            f'{reverse("room-list")}?username={user.username}',
        )

        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()) == 0
