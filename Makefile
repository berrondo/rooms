USERID=$(shell id -u)
GROUPID=$(shell id -g)

setup: lock build

requirements_output = requirements.txt requirements-dev.txt

lock: $(requirements_output)

build_base_image:
	docker build --tag=tauria-challenge --file=Dockerfile .

build: build_base_image
	docker-compose build

$(requirements_output): %txt: %in
	docker run --rm -it -v $(PWD):/app emyller/pip-tools \
		pip-compile -v --generate-hashes --no-index --no-header --output-file $@ $<

migrate:
	./run.sh python manage.py migrate

makemigrations:
	./run.sh python manage.py makemigrations

test:
	./run.sh pytest -vv
