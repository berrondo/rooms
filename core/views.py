from django.core.exceptions import ValidationError as DjangoValidationError
from django.http import HttpResponseBadRequest
from rest_framework import viewsets, mixins, status
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError as DRFValidationError, PermissionDenied
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response

from core.models import User, Room
from core.permission import IsTheUserOrReadOnly
from core.serializers import UserSerializer, RoomSerializer


class ReadOnlyUserViewSet(viewsets.ModelViewSet):
    permission_classes = (IsTheUserOrReadOnly,)
    queryset = User.objects.all()
    serializer_class = UserSerializer
    lookup_field = 'username'


class RoomViewSet(
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    lookup_field = 'guid'
    serializer_class = RoomSerializer

    def get_queryset(self):
        queryset = Room.objects.all()
        username = self.request.query_params.get('username', None)
        if username:
            queryset = queryset.filter(participants__username=username)
        return queryset

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['user'] = self.request.user
        return context

    @action(methods=['PATCH'], detail=True)
    def join(self, request, guid=None):
        room = self.get_object()
        try:
            room.add_user(request.user)
            return Response(status=status.HTTP_200_OK)
        except DjangoValidationError as err:
            raise DRFValidationError(err.message)

    @action(methods=['PATCH'], detail=True)
    def leave(self, request, guid=None):
        room = self.get_object()

        room.remove_user(request.user)
        return Response(status=status.HTTP_200_OK)

    @action(methods=['PATCH'], detail=True)
    def host(self, request, guid=None):
        room = self.get_object()

        if room.host != request.user:
            raise PermissionDenied

        host = get_object_or_404(User, username=request.data['new_host'])
        if host not in room.participants.all():
            return HttpResponseBadRequest('The new host needs to be a participant of this room')

        room.host = host
        room.save()

        return Response(status=status.HTTP_200_OK)
