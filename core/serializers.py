from dj_rest_auth.registration.serializers import RegisterSerializer
from django.contrib.auth.hashers import make_password
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from core.models import User, Room


class RegisterSerializerWithMobileToken(RegisterSerializer):
    mobile_token = serializers.CharField(
        allow_blank=True,
        allow_null=False,
        required=False,
    )
    email = None

    def custom_signup(self, request, user):
        user.mobile_token = self.validated_data.get('mobile_token', '')
        user.save()


class UserSerializer(serializers.ModelSerializer):

    username = serializers.CharField(read_only=True)
    mobile_token = serializers.CharField(write_only=True)
    password = serializers.CharField(write_only=True)

    def validate_password(self, data):
        if data:
            data = make_password(data)
        return data

    class Meta:
        model = User
        fields = ('username', 'mobile_token', 'password')


class RoomSerializer(serializers.ModelSerializer):
    host = serializers.CharField(source='host.username', read_only=True)
    guid = serializers.UUIDField(read_only=True)
    participants = serializers.StringRelatedField(many=True, read_only=True)

    def validate_capacity(self, data):
        if isinstance(data, int) and data <= 0:
            raise ValidationError('You need to have a positive capacity')
        return data

    def create(self, validated_data):
        return Room.create_room(
            name=validated_data['name'],
            host=self.context['user'],
            capacity=validated_data.get('capacity')
        )

    class Meta:
        model = Room
        fields = (
            'name',
            'guid',
            'host',
            'participants',
            'capacity',
        )
